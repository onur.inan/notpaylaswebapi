﻿using Microsoft.Ajax.Utilities;
using Newtonsoft.Json.Linq;
using OnlineNote.Controllers.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;

namespace OnlineNote.Controllers
{
    [BasicAuthentication]
    public class LoginController : ApiController
    {
        OnlineNoteEntities db = new OnlineNoteEntities();



        [Route("api/Login/Test"), HttpGet]
        public string Test()
        {
            return "text";
        }

        

        [Route("api/Login/Login"), HttpPost]
        public LoginResponse Login([FromBody] JObject data)
        {
            string email = data["email"].ToString();
            string password = Password.CreateMD5(data["password"].ToString());

            tbl_Users user = db.tbl_Users.Where(w => w.email == email && w.password == password).FirstOrDefault();

            LoginResponse response = new LoginResponse();
            if (user == null)
            {
                response.sucsess = false;
                response.message = "Hatalı kullanıcı adı ya da şifre";
            }
            else
            {
                response.userID = user.userID;
                response.userName = user.userName;
                response.userSurname = user.userSurname;
                response.sucsess = true;
                response.message = "Kullanıcı girişi başarılı";
            }
            return response;
        }

       

        [Route("api/Login/UpsertRegister"), HttpPost]
        public BaseResponse UpsertRegister([FromBody] JObject data)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                int userID = Convert.ToInt32(data["userID"].ToString());
                string userName = data["userName"].ToString();
                string userSurname = data["userSurname"].ToString();
                string email = data["email"].ToString();
                string password = Password.CreateMD5(data["password"].ToString());

                string message;

                if (userID == 0)
                {
                    tbl_Users insertRegister = new tbl_Users();
                    //insert
                    insertRegister.userID = 0;
                    insertRegister.userName = userName;
                    insertRegister.userSurname = userSurname;
                    insertRegister.email = email;
                    insertRegister.createDate = DateTime.Now;
                    insertRegister.password = password;

                    tbl_Users tbl_Users = db.tbl_Users.Add(insertRegister);
                    db.SaveChanges();
                    message = "Kullanici ekleme işlemi başarılı";

                }
                else
                {
                    tbl_Users updateRegister = db.tbl_Users.Where(w => w.userID == userID).FirstOrDefault(); //update


                    updateRegister.userName = userName;
                    updateRegister.userSurname = userSurname;
                    updateRegister.email = email;
                    updateRegister.createDate = DateTime.Now;
                    updateRegister.password = password;

                    db.SaveChanges();
                    message = "Kullanici güncelleme işlemi başarılı";

                }
                response.sucsess = true;
                response.message = message;
                return response;
            }
            catch (Exception e)
            {
                response.sucsess = false;
                response.message = e.ToString();
                return response;
            }


        }

        [Route("api/Login/TextDelete"), HttpPost]
        public BaseResponse TextDelete([FromBody] JObject data)
        {
            BaseResponse response = new BaseResponse();

            int textID = Convert.ToInt32(data["textID"].ToString());
            tbl_Texts text = db.tbl_Texts.Where(w => w.textID == textID).FirstOrDefault();
            if (text != null)
            {
                db.tbl_Texts.Remove(text);
                db.SaveChanges();
                response.sucsess = true;
                response.message = "Not silme işlemi başarılı";
                return response;
            }
            else
            {
                response.sucsess = false;
                response.message = "Text bulunamadı";
                return response;
            }
            
        }

        [Route("api/Login/GetText"), HttpPost]
        public InfoTextResponse GetText([FromBody] JObject data)
        {
            string guid = data["guid"].ToString();

            tbl_Texts text = db.tbl_Texts.Where(w => w.guid == guid).FirstOrDefault();

            InfoTextResponse response = new InfoTextResponse();

            if (text != null)
            {
                List<tbl_Texts> texts = new List<tbl_Texts>();
                texts.Add(text);
                response.InfoTexts = texts;
                response.sucsess = true;
                response.message = "Not başarıyla getirildi.";
                return response;
            }
            else
            {
                response.sucsess = false;
                response.message = "Not bulunamadı";
                return response;
            }
        }

        [Route("api/Login/GetMyTexts"), HttpPost]
        public InfoTextResponse GetMyTexts([FromBody] JObject data)
        {
            int userID = Convert.ToInt32(data["userID"].ToString());
            List<tbl_Texts> texts = db.tbl_Texts.Where(w => w.userID == userID).ToList();

            InfoTextResponse response = new InfoTextResponse();
            response.InfoTexts = texts;
            response.sucsess = true;
            response.message = "Kullanıcı text kayıtları başarılı bir şekilde listelenmiştir.";
            return response;
        }

        [Route("api/Login/UpsertText"), HttpPost]
        public BaseResponse UpsertText([FromBody] JObject data)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                string message;
                int textID = Convert.ToInt32(data["textID"].ToString());
                int userID = Convert.ToInt32(data["userID"].ToString());
                int ownerID = Convert.ToInt32(data["ownerID"].ToString());
                string textContent = data["textContent"].ToString();
                int size = Convert.ToInt32(data["size"].ToString());
                string background = data["background"].ToString();

                if (textID == 0)
                {
                    tbl_Texts inserttext = new tbl_Texts();
                    inserttext.userID = userID;
                    inserttext.textContent = textContent;
                    inserttext.size = size;
                    inserttext.background = background;
                    inserttext.createDate = DateTime.Now;
                    inserttext.guid = Guid.NewGuid().ToString();

                    db.tbl_Texts.Add(inserttext);
                    db.SaveChanges();
                    message = "Text ekleme işlemi başarılı bir şekilde gerçekleştirilmiştir";

                }
                else
                {
                    tbl_Texts updatetext = db.tbl_Texts.Where(w => w.textID == textID).FirstOrDefault();
                    updatetext.userID = userID;
                    updatetext.textContent = textContent;
                    updatetext.size = size;
                    updatetext.background = background;

                    db.SaveChanges();
                    message = "Text güncelleme işlemi başarılı bir şekilde gerçekleştirilmiştir";

                }

                response.message = message;
                response.sucsess = true;
                return response;
            }

            catch (Exception e)
            {
                response.sucsess = false;
                response.message = e.ToString();
                return response;


            }



        }

        [Route("api/Login/ResetPassword"), HttpPost]
        public BaseResponse ResetPassword([FromBody] JObject data)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                string email = data["email"].ToString();
                string password = data["password"].ToString();
                string message;

                tbl_Users user = db.tbl_Users.Where(w => w.email == email).FirstOrDefault();
                if (user == null)
                {
                    message = "Böyle bir kullanıcı bulunmamaktadır";
                }
                else
                {

                    user.password = password;
                    db.SaveChanges();
                    message = "Şifre değiştirme işlemi başarılı";

                }
                response.sucsess = true;
                response.message = message;
                return response;
            }


            catch (Exception e)
            {
                response.sucsess = false;
                response.message = e.ToString();
                return response;
            }
        }

        [Route("api/Login/ShareText"), HttpPost]
        public BaseResponse ShareText([FromBody] JObject data)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                int textID = Convert.ToInt32(data["textID"]);
                int fromID = Convert.ToInt32(data["fromID"]);       
                int toID = Convert.ToInt32(data["toID"]);

                tbl_Shares shares = new tbl_Shares();
                shares.textID = textID;
                shares.fromID = fromID;
                shares.toID = toID;

                db.tbl_Shares.Add(shares);
                db.SaveChanges();

                response.sucsess = true;
                response.message = "Paylaşma linki başarılı bir şekilde oluşturuldu";
                return response;
            }
            catch (Exception e)
            {
                response.sucsess = false;
                response.message = e.ToString();
                return response;
            }


        }

        [Route("api/Login/GetMySharings"), HttpPost]
        public GetMySharingsResponse GetMySharings([FromBody] JObject data)
        {
            GetMySharingsResponse response = new GetMySharingsResponse();
            try
            {
                int userID = Convert.ToInt32(data["userID"]);

                var shares = db.tbl_Shares.Where(w => w.toID == userID).ToList();
                List<Sharing> sharings = new List<Sharing>();
                for (int i = 0; i < shares.Count; i++)
                {
                    tbl_Shares share = shares[i];

                    tbl_Users user = db.tbl_Users.Where(w => w.userID == share.fromID).FirstOrDefault();
                    tbl_Texts text = db.tbl_Texts.Where(w => w.textID == share.textID).FirstOrDefault();

                    Sharing s = new Sharing();
                    s.ownerVisibleName = user.userName + " " + user.userSurname;
                    s.textID = text.textID;
                    s.textContent = text.textContent;
                    s.createDate = text.createDate;
                    s.size = text.size;
                    s.background = text.background;
                }

                response.sucsess = true;
                response.message = "";
                response.sharings = sharings;

                return response;
            }
            catch (Exception e)
            {
                response.sucsess = false;
                response.message = e.ToString();
                return response;
            }


        }

    }
}
