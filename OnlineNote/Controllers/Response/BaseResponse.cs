﻿namespace OnlineNote.Controllers.Response
{
    public class BaseResponse
    {
        public bool sucsess { get; set; }
        public string message { get; set; }
    }
}