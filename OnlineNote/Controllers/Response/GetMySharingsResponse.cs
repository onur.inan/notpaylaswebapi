﻿using System;
using System.Collections.Generic;

namespace OnlineNote.Controllers.Response
{
    public class GetMySharingsResponse : BaseResponse
    {
        public List<Sharing> sharings { get; set; }
    }

    public class Sharing
    {
        public string ownerVisibleName { get; set; }
        public int textID { get; set; }
        public string textContent { get; set; }
        public Nullable<DateTime> createDate { get; set; }
        public Nullable<int> size { get; set; }
        public string background { get; set; }

    }
}