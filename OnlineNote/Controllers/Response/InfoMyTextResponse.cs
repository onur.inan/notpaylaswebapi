﻿using System;

namespace OnlineNote.Controllers.Response
{
    public class InfoMyTextResponse : BaseResponse
    {
        public int textID { get; set; }
        public int userID { get; set; }
        public Nullable<int> ownerID { get; set; }
        public string textContent { get; set; }
        public Nullable<System.DateTime> createDate { get; set; }
        public int size { get; set; }
        public string background { get; set; }
    }
}