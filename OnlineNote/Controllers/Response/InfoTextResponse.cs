﻿using System;
using System.Collections.Generic;

namespace OnlineNote.Controllers.Response
{
    public class InfoTextResponse : BaseResponse
    {
        public List<tbl_Texts> InfoTexts { get; set; }

    }
}
