﻿namespace OnlineNote.Controllers.Response
{
    public class LoginResponse : BaseResponse
    {
        public int userID;
        public string userName;
        public string userSurname;

    }
}