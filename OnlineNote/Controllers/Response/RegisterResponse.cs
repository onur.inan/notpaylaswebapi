﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineNote.Controllers.Response
{
    public class RegisterResponse:BaseResponse
    {
        public int userID { get; set; }
        public string userName { get; set; }
        public string userSurname { get; set; }
        public string email { get; set; }
        public Nullable<System.DateTime> createDate { get; set; }
        public string password { get; set; }
    }
}
